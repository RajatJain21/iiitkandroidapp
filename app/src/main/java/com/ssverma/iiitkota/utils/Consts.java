package com.ssverma.iiitkota.utils;

/**
 * Created by IIITK on 6/7/2016.
 */
public class Consts {
    public static abstract class Faculty_Constants {
        public final static String CS_DEPARTMENT = "Dept. of Computer Science & Engineering";
        public final static String ECE_DEPARTMENT = "Dept. of Electronics & Comm. Engineering";
    }
    //UG and PG for programs
    public static abstract class Program_Constants{
        public final static String UG_PROGRAMS="UG";
        public final static String PG_PROGRAMS="PG";
    }

}
