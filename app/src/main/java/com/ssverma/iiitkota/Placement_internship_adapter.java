package com.ssverma.iiitkota;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by IIITK on 6/13/2016.
 */
public class Placement_internship_adapter  extends RecyclerView.Adapter<Placement_internship_adapter.ViewHolder> {


    private Context context;
    private ArrayList<Placement_internship_wrapper> listPlace;

    private RCVClickListener listener;

    Placement_internship_adapter(Context context, ArrayList<Placement_internship_wrapper> listPlace) {
        this.context = context;
        this.listPlace = listPlace;
    }

    public void setOnRCVClickListener(RCVClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.internship_module_row_item, parent, false);
        ViewHolder holder = new ViewHolder(rootView);
        return holder;
    }


    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.int_session.setText(listPlace.get(position).getSession());

    }




    @Override
    public int getItemCount() {
        return listPlace.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RelativeLayout itemHolder;
        TextView int_session;


        public ViewHolder(View itemView) {
            super(itemView);

            itemHolder = (RelativeLayout) itemView.findViewById(R.id.int_row_item_holder);
            itemHolder.setOnClickListener(this);
            int_session = (TextView) itemView.findViewById(R.id.session_name);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onRCVClick(v, getAdapterPosition());
            }
        }
    }
}
