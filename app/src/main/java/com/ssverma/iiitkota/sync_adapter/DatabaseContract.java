package com.ssverma.iiitkota.sync_adapter;

import android.net.Uri;

/**
 * Created by IIITK on 6/6/2016.
 */
public class DatabaseContract {
    public static final String DB_NAME = "demo.db";
    public static final String AUTHORITY = "com.ssverma.iiitkota.sync_adapter.IIITK_ContentProvider";
    private static final String CONTENT_TYPE = "vnd.android.cursor.dir/";
    private static final String CONTENT_TYPE_ID = "vnd.android.cursor.item/";

    public static final Uri FACULTY_CONTENT_URI = Uri.parse("content://" + DatabaseContract.AUTHORITY + "/" + DatabaseContract.FacultyTable.TABLE_NAME);
    //Programs URI
    public static final Uri PROGRAM_CONTENT_URI=Uri.parse("content://"+DatabaseContract.AUTHORITY+"/"+DatabaseContract.ProgramTable.TABLE_NAME);
    //VC URI
    public static final Uri VC_CONTENT_URI=Uri.parse("content://"+DatabaseContract.AUTHORITY+"/"+DatabaseContract.Placement_Visting_Company_Table.TABLE_NAME);
    public static final Uri RP_CONTENT_URI=Uri.parse("content://"+DatabaseContract.AUTHORITY+"/"+DatabaseContract.Placement_RP_Table.TABLE_NAME);
    public static final Uri PLT_CONTENT_URI=Uri.parse("content://"+DatabaseContract.AUTHORITY+"/"+DatabaseContract.Placement_module_Table.TABLE_NAME);
    public static final Uri INT_CONTENT_URI=Uri.parse("content://"+DatabaseContract.AUTHORITY+"/"+DatabaseContract.Internship_module_Table.TABLE_NAME);

    public abstract class FacultyTable {

        public static final String FACULTY_CONTENT_TYPE =
                CONTENT_TYPE + "vnd.Demo_ContentProvider.faculty";
        public static final String FACULTY_CONTENT_TYPE_ID =
                CONTENT_TYPE_ID + "vnd.Demo_ContentProvider.faculty";

        public static final String TABLE_NAME = "faculty_table";
        public static final String FACULTY_SERVER_ID = "id";
        public static final String FACULTY_NAME = "Name";
        public static final String FACULTY_ID = "FacultyId";
        public static final String FACULTY_EMAIL = "Email";
        public static final String FACULTY_DOB = "DateOfBirth";
        public static final String FACULTY_DEPARTMENT = "Department";
        public static final String FACULTY_CONTACT = "Contact";
        public static final String FACULTY_IMAGE = "Image";
        public static final String FACULTY_QUALIFICATION = "Qualification";
        public static final String FACULTY_HOMETOWN = "Hometown";
        public static final String FACULTY_DESIGNATION = "Designation";
        public static final String FACULTY_ACHIEVEMENTS = "Achievements";
        public static final String FACULTY_SUMMARY = "Summary";
        public static final String FACULTY_RESEARCH_AREAS = "ResearchAreas";
        public static final String FACULTY_FACEBOOK = "Facebook";

    }

    //Programs Module By rajat Kumar jain--08/06/16
    public abstract class ProgramTable{
        public static final String PROGRAM_CONTENT_TYPE=CONTENT_TYPE+ "vnd.Demo_ContentProvider.program";;
        public static final String PROGRAM_CONTENT_TYPE_ID =
                CONTENT_TYPE_ID + "vnd.Demo_ContentProvider.program";
        public static final String TABLE_NAME = "program";
        public static final String PROGRAM_SERVER_ID = "id";
        public static final String PROGRAM_NAME = "Program_name";
        public static final String PROGRAM_Type = "Program_type";
        public static final String PROGRAM_SEAT = "Program_seats";
        public static final String PROGRAM_DESC = "Program_desc";
        public static final String PROGRAM_IMAGE = "Program_image";
        public static final String PROGRAM_ELIGIBILITY = "Program_eli";
        public static final String PROGRAM_DURATION = "Program_duration";
        public static final String PROGRAM_FEE = "Program_fee";

    }
//Visiting Company's Module-Rajat jain
    public abstract class Placement_Visting_Company_Table
    {
        public static final String VC_CONTENT_TYPE=CONTENT_TYPE+ "vnd.Demo_ContentProvider.placement_vc";;
        public static final String VC_CONTENT_TYPE_ID =
                CONTENT_TYPE_ID + "vnd.Demo_ContentProvider.placement_vc";
        public static final String TABLE_NAME="company_profile";
        public static final String VC_SERVER_ID = "id";
        public static final String VC_NAME = "Name";
        public static final String VC_SUMMARY = "Summary";
        public static final String VC_EMAIL = "Email";
        public static final String VC_ADDRESS = "Address";
        public static final String VC_CONTACT = "Contact";
        public static final String VC_INDUSTRY = "Industry";
        public static final String VC_DOMAIN = "Domain";
        public static final String VC_CTC = "Expected_CTC";
        public static final String VC_STRENGTH= "Strength";
        public static final String VC_TURNOVER = "TurnOver";
        public static final String VC_IMAGE= "Image";

    }
//Created by Rajat Jain
    public abstract class Placement_RP_Table
    {
        public static final String RP_CONTENT_TYPE=CONTENT_TYPE+ "vnd.Demo_ContentProvider.placement_rp";;
        public static final String RP_CONTENT_TYPE_ID =
                CONTENT_TYPE_ID + "vnd.Demo_ContentProvider.placement_rp";
        public static final String TABLE_NAME   ="contacts_rp";
        public static final String RP_SERVER_ID ="contact_id";
        public static final String RP_NAME      ="contact_name";
        public static final String RP_DES       ="contact_designation";
        public static final String RP_EMAIL     ="contact_email";
        public static final String RP_MOB       ="contact_mobile_no";
        public static final String RP_CATEGORY  ="contact_category";
        public static final String RP_IMAGE     ="Contact_image";
    }
//Created By rajat kumar jain   13/06/15
    public abstract  class Placement_module_Table{
    public static final String PLT_CONTENT_TYPE=CONTENT_TYPE+ "vnd.Demo_ContentProvider.placement";;
    public static final String PLT_CONTENT_TYPE_ID =
            CONTENT_TYPE_ID + "vnd.Demo_ContentProvider.placement";
    public static final String TABLE_NAME    ="placement_data";
    public static final String PLT_SERVER_ID ="id";
    public static final String PLT_SNAME     ="student_name";
    public static final String PLT_COMPNAY   ="company_name";
    public static final String PLT_BRANCH    ="branch";
    public static final String PLT_PACKAGE   ="package";
    public static final String PLT_SESSION   ="session";


}

    //Created By rajat kumar jain   13/06/15
    public abstract  class Internship_module_Table{
        public static final String INT_CONTENT_TYPE=CONTENT_TYPE+ "vnd.Demo_ContentProvider.internship";;
        public static final String INT_CONTENT_TYPE_ID =
                CONTENT_TYPE_ID + "vnd.Demo_ContentProvider.internship";
        public static final String TABLE_NAME   ="internship_Data";
        public static final String INT_SERVER_ID ="id";
        public static final String INT_SNAME      ="student_name";
        public static final String INT_COMPNAY    ="company_name";
        public static final String INT_BRANCH     ="branch";
        public static final String INT_STIPEND    ="stipend";
        public static final String INT_DETAILS    ="details";
        public static final String INT_SESSION    ="session";

    }


}
