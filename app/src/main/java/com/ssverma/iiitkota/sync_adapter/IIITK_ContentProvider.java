package com.ssverma.iiitkota.sync_adapter;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;

/**
 * Created by IIITK on 6/6/2016.
 */
public class IIITK_ContentProvider extends ContentProvider{


    private static final UriMatcher uriMatcher;

    private static final int FACULTY_TABLE = 0;  // All rows
    private static final int FACULTY_TABLE_ROW = 1;  //Single row;

    //Programs
    private static final int PROGRAM_TABLE=2;
    private static final int PROGRAM_TABLE_ROW=3;

    //VC Module-Rajat jain
    private static final int VC_TABLE=12;
    private static final int VC_TABLE_ROW=13;
    //RP Module -Rajat jain
    private static final int RP_TABLE=16;
    private static final int RP_TABLE_ROW=17;
    //Placement Module-rajat jain 13/06/16
    private static final int PLT_TABLE=18;
    private static final int PLT_TABLE_ROW=19;
    //Internship Module -rajat jain 13/06/16
    private static final int INT_TABLE=20;
    private static  final int INT_TABLE_ROW=21;


    private DatabaseHelper databaseHelper;
    private SQLiteDatabase db;


    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.FacultyTable.TABLE_NAME , FACULTY_TABLE);
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.FacultyTable.TABLE_NAME + "/#" , FACULTY_TABLE_ROW);

        //Programs
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.ProgramTable.TABLE_NAME , PROGRAM_TABLE);
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.ProgramTable.TABLE_NAME + "/#" ,PROGRAM_TABLE_ROW);

        //VC Module
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.Placement_Visting_Company_Table.TABLE_NAME , VC_TABLE);
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.Placement_Visting_Company_Table.TABLE_NAME + "/#" ,VC_TABLE_ROW);


        //RP Module
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.Placement_RP_Table.TABLE_NAME ,RP_TABLE);
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.Placement_RP_Table.TABLE_NAME + "/#" ,RP_TABLE_ROW);

        //Placement Module
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.Placement_module_Table.TABLE_NAME ,PLT_TABLE);
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.Placement_module_Table.TABLE_NAME + "/#" ,PLT_TABLE_ROW);

        //Internship Module
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.Internship_module_Table.TABLE_NAME ,INT_TABLE);
        uriMatcher.addURI(DatabaseContract.AUTHORITY , DatabaseContract.Internship_module_Table.TABLE_NAME + "/#" ,INT_TABLE_ROW);
    }


    @Override
    public boolean onCreate() {
        databaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        db = databaseHelper.getReadableDatabase();

        Cursor cursor;

        switch (uriMatcher.match(uri)){
            case FACULTY_TABLE :
                cursor = db.query(DatabaseContract.FacultyTable.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver() , uri);
                return cursor;

            case FACULTY_TABLE_ROW :
                selection = "_ID LIKE " + uri.getLastPathSegment();
                return db.query(DatabaseContract.FacultyTable.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);

            //Add program Module Case
            case PROGRAM_TABLE:
                cursor = db.query(DatabaseContract.ProgramTable.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver() , uri);
                return cursor;

            case PROGRAM_TABLE_ROW:
                selection = "_ID LIKE " + uri.getLastPathSegment();
                return db.query(DatabaseContract.ProgramTable.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);

            //Add VC Module
            case VC_TABLE:
                cursor = db.query(DatabaseContract.Placement_Visting_Company_Table.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver() , uri);
                return cursor;

            case VC_TABLE_ROW:
                selection = "_ID LIKE " + uri.getLastPathSegment();
                return db.query(DatabaseContract.Placement_Visting_Company_Table.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
            //Add RP Module
            case RP_TABLE:
                cursor = db.query(DatabaseContract.Placement_RP_Table.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver() , uri);
                return cursor;

            case RP_TABLE_ROW:
                selection = "_ID LIKE " + uri.getLastPathSegment();
                return db.query(DatabaseContract.Placement_RP_Table.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
            //Placement Module-Rajat Kr. jain -13/06/16
            case PLT_TABLE:
                cursor = db.query(DatabaseContract.Placement_module_Table.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver() , uri);
                return cursor;

            case PLT_TABLE_ROW:
                selection = "_ID LIKE " + uri.getLastPathSegment();
                return db.query(DatabaseContract.Placement_module_Table.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);

            //Internship Module-Rajat Kr. jain -13/06/16
            case INT_TABLE:
                cursor = db.query(DatabaseContract.Internship_module_Table.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver() , uri);
                return cursor;

            case INT_TABLE_ROW:
                selection = "_ID LIKE " + uri.getLastPathSegment();
                return db.query(DatabaseContract.Internship_module_Table.TABLE_NAME , projection , selection , selectionArgs , null , null , sortOrder);
        }

        return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {

        switch (uriMatcher.match(uri)){
            case FACULTY_TABLE:
                return DatabaseContract.FacultyTable.FACULTY_CONTENT_TYPE;
            case FACULTY_TABLE_ROW:
                return DatabaseContract.FacultyTable.FACULTY_CONTENT_TYPE_ID;

            //program Module
            case PROGRAM_TABLE:
                return DatabaseContract.ProgramTable.PROGRAM_CONTENT_TYPE;
            case PROGRAM_TABLE_ROW:
                return DatabaseContract.ProgramTable.PROGRAM_CONTENT_TYPE_ID;

            //visiting companyModule
            case VC_TABLE:
                return DatabaseContract.Placement_Visting_Company_Table.VC_CONTENT_TYPE;
            case VC_TABLE_ROW:
                return DatabaseContract.Placement_Visting_Company_Table.VC_CONTENT_TYPE_ID;

            //Representative Module
            case RP_TABLE:
                return DatabaseContract.Placement_RP_Table.RP_CONTENT_TYPE;
            case RP_TABLE_ROW:
                return DatabaseContract.Placement_RP_Table.RP_CONTENT_TYPE_ID;
            //Placement  Module 13/06/16
            case PLT_TABLE:
                return DatabaseContract.Placement_module_Table.PLT_CONTENT_TYPE;
            case PLT_TABLE_ROW:
                return DatabaseContract.Placement_module_Table.PLT_CONTENT_TYPE_ID;
            //Internship  Module 13/06/16
            case INT_TABLE:
                return DatabaseContract.Internship_module_Table.INT_CONTENT_TYPE;
            case INT_TABLE_ROW:
                return DatabaseContract.Internship_module_Table.INT_CONTENT_TYPE_ID;

        }

        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        db = databaseHelper.getWritableDatabase();

        switch (uriMatcher.match(uri)) {
            case FACULTY_TABLE:
                long row_id = db.insert(DatabaseContract.FacultyTable.TABLE_NAME , null , values);
                Uri _uri = ContentUris.withAppendedId(uri , row_id);
                //getContext().getContentResolver().notifyChange(_uri , null);
                return _uri;

            //case Program
            case PROGRAM_TABLE:
                long row_id_pr = db.insert(DatabaseContract.ProgramTable.TABLE_NAME , null , values);
                Uri PR_uri = ContentUris.withAppendedId(uri , row_id_pr);
                //getContext().getContentResolver().notifyChange(_uri , null);
                return PR_uri;

            //case VC Module
            case VC_TABLE:
                long row_id_vc = db.insert(DatabaseContract.Placement_Visting_Company_Table.TABLE_NAME , null , values);
                Uri VC_uri = ContentUris.withAppendedId(uri , row_id_vc);
                //getContext().getContentResolver().notifyChange(_uri , null);
                return VC_uri;

            //case RP Module
            case RP_TABLE:
                long row_id_rp= db.insert(DatabaseContract.Placement_RP_Table.TABLE_NAME , null , values);
                Uri RP_uri = ContentUris.withAppendedId(uri , row_id_rp);
                //getContext().getContentResolver().notifyChange(_uri , null);
                return RP_uri;

            //case Placement Module
            case PLT_TABLE:
                long row_id_plt=db.insert(DatabaseContract.Placement_module_Table.TABLE_NAME,null,values);
                Uri PLT_Uri=ContentUris.withAppendedId(uri,row_id_plt);
                return PLT_Uri;
            //case Internship Module
            case INT_TABLE:
                long row_id_int=db.insert(DatabaseContract.Internship_module_Table.TABLE_NAME,null,values);
                Uri INT_Uri=ContentUris.withAppendedId(uri,row_id_int);
                return INT_Uri;
        }

        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        db = databaseHelper.getWritableDatabase();
        int updatedRows;
        switch (uriMatcher.match(uri)){
            case FACULTY_TABLE:
                 updatedRows = db.update(DatabaseContract.FacultyTable.TABLE_NAME , values , selection , selectionArgs);
                return updatedRows;

            //update for Program
            case PROGRAM_TABLE:
                updatedRows=db.update(DatabaseContract.ProgramTable.TABLE_NAME,values,selection,selectionArgs);
                return updatedRows;
            //update for VC
            case VC_TABLE:
                updatedRows=db.update(DatabaseContract.Placement_Visting_Company_Table.TABLE_NAME,values,selection,selectionArgs);
                return updatedRows;

            //update for RP
            case RP_TABLE:
                updatedRows=db.update(DatabaseContract.Placement_RP_Table.TABLE_NAME,values,selection,selectionArgs);
                return updatedRows;

            //Placement Module
            case PLT_TABLE:
                updatedRows=db.update(DatabaseContract.Placement_module_Table.TABLE_NAME,values,selection,selectionArgs);
                return updatedRows;

            //Intenship Module
            case INT_TABLE:
                updatedRows=db.update(DatabaseContract.Internship_module_Table.TABLE_NAME,values,selection,selectionArgs);
                return updatedRows;
        }

        return 0;
    }
}