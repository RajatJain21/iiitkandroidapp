package com.ssverma.iiitkota.sync_adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by IIITK on 6/6/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA = ", ";

    private static final String CREATE_FACULTY_TABLE = "CREATE TABLE "
            + DatabaseContract.FacultyTable.TABLE_NAME + " ("
            + DatabaseContract.FacultyTable.FACULTY_SERVER_ID + INTEGER_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_NAME + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_ID + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_EMAIL + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_DOB + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_DEPARTMENT + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_CONTACT + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_IMAGE + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_QUALIFICATION + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_HOMETOWN + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_DESIGNATION + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_ACHIEVEMENTS + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_SUMMARY + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_RESEARCH_AREAS + TEXT_TYPE + COMMA
            + DatabaseContract.FacultyTable.FACULTY_FACEBOOK + TEXT_TYPE + " )";

    private static final String DROP_FACULTY_TABLE = "DROP TABLE IF EXISTS "
            +DatabaseContract.FacultyTable.TABLE_NAME;


    //Programs Module By Rajat Kumar Jain 08-06-16

    private static final String CREATE_PROGRAM_TABLE = "CREATE TABLE "
            + DatabaseContract.ProgramTable.TABLE_NAME + " ("
            + DatabaseContract.ProgramTable.PROGRAM_SERVER_ID + INTEGER_TYPE + COMMA
            + DatabaseContract.ProgramTable.PROGRAM_NAME + TEXT_TYPE + COMMA
            + DatabaseContract.ProgramTable.PROGRAM_DESC + TEXT_TYPE + COMMA
            + DatabaseContract.ProgramTable.PROGRAM_ELIGIBILITY + TEXT_TYPE + COMMA
            + DatabaseContract.ProgramTable.PROGRAM_Type+ TEXT_TYPE + COMMA
            + DatabaseContract.ProgramTable.PROGRAM_DURATION + TEXT_TYPE + COMMA
            + DatabaseContract.ProgramTable.PROGRAM_FEE + TEXT_TYPE + COMMA
            + DatabaseContract.ProgramTable.PROGRAM_IMAGE + TEXT_TYPE + COMMA
            + DatabaseContract.ProgramTable.PROGRAM_SEAT+TEXT_TYPE
            + " )";

    private static final String DROP_PROGRAM_TABLE = "DROP TABLE IF EXISTS "
            +DatabaseContract.ProgramTable.TABLE_NAME;

    //Visiting Company's Module-Rajat jain
    private static final String CREATE_VC_TABLE = "CREATE TABLE "
            + DatabaseContract.Placement_Visting_Company_Table.TABLE_NAME + " ("
            + DatabaseContract.Placement_Visting_Company_Table.VC_SERVER_ID + INTEGER_TYPE + COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_CTC + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_INDUSTRY + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_CONTACT+ TEXT_TYPE + COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_DOMAIN+ TEXT_TYPE + COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_EMAIL + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_IMAGE + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_NAME + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_ADDRESS+TEXT_TYPE+COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_STRENGTH+TEXT_TYPE+COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_SUMMARY+TEXT_TYPE+COMMA
            + DatabaseContract.Placement_Visting_Company_Table.VC_TURNOVER+TEXT_TYPE
            + " )";

    private static final String DROP_VC_TABLE = "DROP TABLE IF EXISTS "
            +DatabaseContract.Placement_Visting_Company_Table.TABLE_NAME;


    //Representative Module-Rajat jain

    private static final String CREATE_RP_TABLE = "CREATE TABLE "
            + DatabaseContract.Placement_RP_Table.TABLE_NAME + " ("
            + DatabaseContract.Placement_RP_Table.RP_SERVER_ID + INTEGER_TYPE + COMMA
            + DatabaseContract.Placement_RP_Table.RP_DES + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_RP_Table.RP_EMAIL + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_RP_Table.RP_MOB+ TEXT_TYPE + COMMA
            + DatabaseContract.Placement_RP_Table.RP_NAME+ TEXT_TYPE + COMMA
            + DatabaseContract.Placement_RP_Table.RP_CATEGORY + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_RP_Table.RP_IMAGE+ TEXT_TYPE
            + " )";

    private static final String DROP_RP_TABLE = "DROP TABLE IF EXISTS "
            +DatabaseContract.Placement_RP_Table.TABLE_NAME;

    //placement module-rajat jain 13-06-2016

    private static final String CREATE_PLT_TABLE = "CREATE TABLE "
            + DatabaseContract.Placement_module_Table.TABLE_NAME + " ("
            + DatabaseContract.Placement_module_Table.PLT_SERVER_ID+ INTEGER_TYPE + COMMA
            + DatabaseContract.Placement_module_Table.PLT_BRANCH + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_module_Table.PLT_COMPNAY + TEXT_TYPE + COMMA
            + DatabaseContract.Placement_module_Table.PLT_PACKAGE+ INTEGER_TYPE + COMMA
            + DatabaseContract.Placement_module_Table.PLT_SESSION+ TEXT_TYPE + COMMA
            + DatabaseContract.Placement_module_Table.PLT_SNAME + TEXT_TYPE
            + " )";

    private static final String DROP_PLT_TABLE = "DROP TABLE IF EXISTS "
            +DatabaseContract.Placement_module_Table.TABLE_NAME;

    //Internships module-rajat jain 13-06-2016

    private static final String CREATE_INTERN_TABLE = "CREATE TABLE "
            + DatabaseContract.Internship_module_Table.TABLE_NAME + " ("
            + DatabaseContract.Internship_module_Table.INT_SERVER_ID+ INTEGER_TYPE + COMMA
            + DatabaseContract.Internship_module_Table.INT_BRANCH + TEXT_TYPE + COMMA
            + DatabaseContract.Internship_module_Table.INT_COMPNAY + TEXT_TYPE + COMMA
            + DatabaseContract.Internship_module_Table.INT_DETAILS+ TEXT_TYPE + COMMA
            + DatabaseContract.Internship_module_Table.INT_SESSION+ TEXT_TYPE + COMMA
            + DatabaseContract.Internship_module_Table.INT_SNAME+ TEXT_TYPE + COMMA
            + DatabaseContract.Internship_module_Table.INT_STIPEND+ TEXT_TYPE
            + " )";

    private static final String DROP_INT_TABLE = "DROP TABLE IF EXISTS "
            +DatabaseContract.Internship_module_Table.TABLE_NAME;


    public DatabaseHelper(Context context) {
        super(context, DatabaseContract.DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_FACULTY_TABLE);
        //programs module
        db.execSQL(CREATE_PROGRAM_TABLE);
        //Visiting Company
        db.execSQL(CREATE_VC_TABLE);
        //RP Company
        db.execSQL(CREATE_RP_TABLE);
        //Placement Module
        db.execSQL(CREATE_PLT_TABLE);
        //Internship Module
        db.execSQL(CREATE_INTERN_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_FACULTY_TABLE);
        //Program-Module
        db.execSQL(DROP_PROGRAM_TABLE);
        //VC Module
        db.execSQL(DROP_VC_TABLE);
        //RP Module
        db.execSQL(DROP_RP_TABLE);
        //Placement Module
        db.execSQL(DROP_PLT_TABLE);
        //Internship Module
        db.execSQL(DROP_INT_TABLE);
        onCreate(db);


    }
}
