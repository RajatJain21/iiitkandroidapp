package com.ssverma.iiitkota;

/**
 * Created by IIITK on 6/13/2016.
 */
public class Placement_internship_wrapper {

    private int id;

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getPlacement_offer() {
        return placement_offer;
    }

    public void setPlacement_offer(String placement_offer) {
        this.placement_offer = placement_offer;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    private String student_name;
    private String company_name;
    private String placement_offer;
    private String session;
    private String branch;
}
